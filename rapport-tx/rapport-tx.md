---
fontsize: 12pt
lang: fr
lieu: UNIVERSITÉ DE SHERBROOKE
title: RAPPORT DE STAGE
presented-to: TODO Prénom Nom du coach, conseillère en développement professionnel
author: |
        | Matthieu Daoust, stagiaire
        | TODO Entreprise
        | TODO Domaine
        | TODO Tx
date: TODO date
geometry: margin=1in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{1.5}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \lfoot{Matthieu Daoust}
    - \cfoot{\thepage}
    - \rfoot{Matthieu.Daoust@USherbrooke.ca}
---

# Partie A: TODO titre qui représente bien le résumé

Présentez votre expérience de stage en quelques lignes comme vous le feriez à un futur employeur.

## Mise en contexte

TODO

## Résumé du mandat

TODO

## Conclusion sur mon expérience de stage

TODO

# Partie B

## L'environnement

> _Décrivez votre milieu de travail, indiquez vos zones de confort et d’inconfort quant à ces conditions et expliquez pourquoi._

Ex.: travailler à l’extérieur, en laboratoire ou dans un bureau, le rythme, la charge de travail, le niveau de stress, la supervision, le travail d’équipe ou en solitaire, etc.

TODO

## Les connaissances

> _Que savez-vous maintenant, qui vous aidera pour vos prochains stages ou votre emploi? Pourquoi?_

TODO

## Le professionnalisme

> _Avez-vous fait des apprentissages particuliers en lien avec certaines règles ou normes et quel comportement avez-vous adopté?_

(règles de sécurité, d’éthique, de confidentialité ou autres?)

TODO

## La connaissance de soi

(préférences, forces, valeurs, limites)

> _Quels intérêts avez-vous découverts ou confirmés lors de cette expérience?_

TODO
